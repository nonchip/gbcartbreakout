EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:doragasu
LIBS:ttl_ieee
LIBS:triac_thyristor
LIBS:transf
LIBS:switches
LIBS:supertex
LIBS:stm8
LIBS:stm32
LIBS:silabs
LIBS:sensors
LIBS:rfcom
LIBS:relays
LIBS:references
LIBS:pspice
LIBS:powerint
LIBS:onsemi
LIBS:nxp_armmcu
LIBS:nxp
LIBS:nordicsemi
LIBS:msp430
LIBS:motors
LIBS:motor_drivers
LIBS:modules
LIBS:microchip_pic32mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic10mcu
LIBS:microchip_dspic33dsc
LIBS:mechanical
LIBS:maxim
LIBS:logic_programmable
LIBS:leds
LIBS:ir
LIBS:intersil
LIBS:infineon
LIBS:hc11
LIBS:graphic_symbols
LIBS:gennum
LIBS:ftdi
LIBS:elec-unifil
LIBS:diode
LIBS:dc-dc
LIBS:cmos_ieee
LIBS:brooktre
LIBS:bosch
LIBS:bbd
LIBS:battery_management
LIBS:analog_devices
LIBS:allegro
LIBS:actel
LIBS:ac-dc
LIBS:Zilog
LIBS:Xicor
LIBS:Worldsemi
LIBS:RFSolutions
LIBS:Power_Management
LIBS:Oscillators
LIBS:Lattice
LIBS:LEM
LIBS:ESD_Protection
LIBS:Altera
LIBS:74xgxx
LIBS:zetex
LIBS:wiznet
LIBS:video
LIBS:gbcartbreakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GB-CART Jfingers1
U 1 1 5895A384
P 2150 3550
F 0 "Jfingers1" H 2050 5250 60  0000 C CNN
F 1 "GB-CART" H 2150 1850 60  0000 C CNN
F 2 "doragasu-footprints:GB_CART_FINGERS" H 2150 3550 60  0001 C CNN
F 3 "" H 2150 3550 60  0000 C CNN
	1    2150 3550
	1    0    0    -1  
$EndComp
$Comp
L GB-CART Jslot1
U 1 1 5895B006
P 9250 3550
F 0 "Jslot1" H 9150 5250 60  0000 C CNN
F 1 "GB-CART" H 9250 1850 60  0000 C CNN
F 2 "doragasu-footprints:GB_CART" H 9250 3550 60  0001 C CNN
F 3 "" H 9250 3550 60  0000 C CNN
	1    9250 3550
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x08 SWal1
U 1 1 59BC4AC3
P 4900 2600
F 0 "SWal1" H 4900 3150 50  0000 C CNN
F 1 "SW_DIP_x08" H 4900 2150 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_DIP_x8_W6.15mm_Slide_Omron_A6H" H 4900 2600 50  0001 C CNN
F 3 "" H 4900 2600 50  0001 C CNN
	1    4900 2600
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x08 SWah1
U 1 1 59BC4B38
P 4900 3750
F 0 "SWah1" H 4900 4300 50  0000 C CNN
F 1 "SW_DIP_x08" H 4900 3300 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_DIP_x8_W6.15mm_Slide_Omron_A6H" H 4900 3750 50  0001 C CNN
F 3 "" H 4900 3750 50  0001 C CNN
	1    4900 3750
	1    0    0    -1  
$EndComp
Text Label 9550 2000 0    60   ~ 0
slot1
Text Label 9550 2100 0    60   ~ 0
slot2
Text Label 9550 2200 0    60   ~ 0
slot3
Text Label 9550 2300 0    60   ~ 0
slot4
Text Label 9550 2400 0    60   ~ 0
slot5
Text Label 9550 2500 0    60   ~ 0
slot6
Text Label 9550 2600 0    60   ~ 0
slot7
Text Label 9550 2700 0    60   ~ 0
slot8
Text Label 9550 2800 0    60   ~ 0
slot9
Text Label 9550 2900 0    60   ~ 0
slot10
Text Label 9550 3000 0    60   ~ 0
slot11
Text Label 9550 3100 0    60   ~ 0
slot12
Text Label 9550 3200 0    60   ~ 0
slot13
Text Label 9550 3300 0    60   ~ 0
slot14
Text Label 9550 3400 0    60   ~ 0
slot15
Text Label 9550 3500 0    60   ~ 0
slot16
Text Label 9550 3600 0    60   ~ 0
slot17
Text Label 9550 3700 0    60   ~ 0
slot18
Text Label 9550 3800 0    60   ~ 0
slot19
Text Label 9550 3900 0    60   ~ 0
slot20
Text Label 9550 4000 0    60   ~ 0
slot21
Text Label 9550 4100 0    60   ~ 0
slot22
Text Label 9550 4200 0    60   ~ 0
slot23
Text Label 9550 4300 0    60   ~ 0
slot24
Text Label 9550 4400 0    60   ~ 0
slot25
Text Label 9550 4500 0    60   ~ 0
slot26
Text Label 9550 4600 0    60   ~ 0
slot27
Text Label 9550 4700 0    60   ~ 0
slot28
Text Label 9550 4800 0    60   ~ 0
slot29
Text Label 10150 4900 0    60   ~ 0
slot30
Text Label 9550 5000 0    60   ~ 0
slot31
Text Label 9550 5100 0    60   ~ 0
slot32
Text Label 7500 2050 0    60   ~ 0
slot1
Text Label 7500 2150 0    60   ~ 0
slot2
Text Label 7500 2250 0    60   ~ 0
slot3
Text Label 7500 2350 0    60   ~ 0
slot4
Text Label 7500 2450 0    60   ~ 0
slot5
Text Label 7500 2550 0    60   ~ 0
slot6
Text Label 7500 2650 0    60   ~ 0
slot7
Text Label 7500 2750 0    60   ~ 0
slot8
Text Label 7500 2850 0    60   ~ 0
slot9
Text Label 7500 2950 0    60   ~ 0
slot10
Text Label 7500 3050 0    60   ~ 0
slot11
Text Label 7500 3150 0    60   ~ 0
slot12
Text Label 7500 3250 0    60   ~ 0
slot13
Text Label 7500 3350 0    60   ~ 0
slot14
Text Label 7500 3450 0    60   ~ 0
slot15
Text Label 7500 3550 0    60   ~ 0
slot16
Text Label 7500 3650 0    60   ~ 0
slot17
Text Label 7500 3750 0    60   ~ 0
slot18
Text Label 7500 3850 0    60   ~ 0
slot19
Text Label 7500 3950 0    60   ~ 0
slot20
Text Label 7500 4050 0    60   ~ 0
slot21
Text Label 7500 4150 0    60   ~ 0
slot22
Text Label 7500 4250 0    60   ~ 0
slot23
Text Label 7500 4350 0    60   ~ 0
slot24
Text Label 7500 4450 0    60   ~ 0
slot25
Text Label 7500 4550 0    60   ~ 0
slot26
Text Label 7500 4650 0    60   ~ 0
slot27
Text Label 7500 4750 0    60   ~ 0
slot28
Text Label 7500 4850 0    60   ~ 0
slot29
Text Label 7500 4950 0    60   ~ 0
slot30
Text Label 7500 5050 0    60   ~ 0
slot31
Text Label 7500 5150 0    60   ~ 0
slot32
Text Label 4300 700  0    60   ~ 0
slot1
Text Label 4300 800  0    60   ~ 0
slot2
Text Label 4300 900  0    60   ~ 0
slot3
Text Label 4300 1000 0    60   ~ 0
slot4
Text Label 4300 1100 0    60   ~ 0
slot5
Text Label 5200 2200 0    60   ~ 0
slot6
Text Label 5200 2300 0    60   ~ 0
slot7
Text Label 5200 2400 0    60   ~ 0
slot8
Text Label 5200 2500 0    60   ~ 0
slot9
Text Label 5200 2600 0    60   ~ 0
slot10
Text Label 5200 2700 0    60   ~ 0
slot11
Text Label 5200 2800 0    60   ~ 0
slot12
Text Label 5200 2900 0    60   ~ 0
slot13
Text Label 5200 3350 0    60   ~ 0
slot14
Text Label 5200 3450 0    60   ~ 0
slot15
Text Label 5200 3550 0    60   ~ 0
slot16
Text Label 5200 3650 0    60   ~ 0
slot17
Text Label 5200 3750 0    60   ~ 0
slot18
Text Label 5200 3850 0    60   ~ 0
slot19
Text Label 5200 3950 0    60   ~ 0
slot20
Text Label 5200 4050 0    60   ~ 0
slot21
$Comp
L SW_DIP_x08 SWd1
U 1 1 59BC5C00
P 4900 4900
F 0 "SWd1" H 4900 5450 50  0000 C CNN
F 1 "SW_DIP_x08" H 4900 4450 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_DIP_x8_W6.15mm_Slide_Omron_A6H" H 4900 4900 50  0001 C CNN
F 3 "" H 4900 4900 50  0001 C CNN
	1    4900 4900
	1    0    0    -1  
$EndComp
Text Label 5200 4500 0    60   ~ 0
slot22
Text Label 5200 4600 0    60   ~ 0
slot23
Text Label 5200 4700 0    60   ~ 0
slot24
Text Label 5200 4800 0    60   ~ 0
slot25
Text Label 5200 4900 0    60   ~ 0
slot26
Text Label 5200 5000 0    60   ~ 0
slot27
Text Label 5200 5100 0    60   ~ 0
slot28
Text Label 5200 5200 0    60   ~ 0
slot29
Text Label 4300 1200 0    60   ~ 0
slot30
Text Label 4300 1300 0    60   ~ 0
slot31
Text Label 4300 1400 0    60   ~ 0
slot32
Text Label 2450 2000 0    60   ~ 0
finger1
Text Label 2450 2100 0    60   ~ 0
finger2
Text Label 2450 2200 0    60   ~ 0
finger3
Text Label 2450 2300 0    60   ~ 0
finger4
Text Label 2450 2400 0    60   ~ 0
finger5
Text Label 2450 2500 0    60   ~ 0
finger6
Text Label 2450 2600 0    60   ~ 0
finger7
Text Label 2450 2700 0    60   ~ 0
finger8
Text Label 2450 2800 0    60   ~ 0
finger9
Text Label 2450 2900 0    60   ~ 0
finger10
Text Label 2450 3000 0    60   ~ 0
finger11
Text Label 2450 3100 0    60   ~ 0
finger12
Text Label 2450 3200 0    60   ~ 0
finger13
Text Label 2450 3300 0    60   ~ 0
finger14
Text Label 2450 3400 0    60   ~ 0
finger15
Text Label 2450 3500 0    60   ~ 0
finger16
Text Label 2450 3600 0    60   ~ 0
finger17
Text Label 2450 3700 0    60   ~ 0
finger18
Text Label 2450 3800 0    60   ~ 0
finger19
Text Label 2450 3900 0    60   ~ 0
finger20
Text Label 2450 4000 0    60   ~ 0
finger21
Text Label 2450 4100 0    60   ~ 0
finger22
Text Label 2450 4200 0    60   ~ 0
finger23
Text Label 2450 4300 0    60   ~ 0
finger24
Text Label 2450 4400 0    60   ~ 0
finger25
Text Label 2450 4500 0    60   ~ 0
finger26
Text Label 2450 4600 0    60   ~ 0
finger27
Text Label 2450 4700 0    60   ~ 0
finger28
Text Label 2450 4800 0    60   ~ 0
finger29
Text Label 2450 4900 0    60   ~ 0
finger30
Text Label 2450 5000 0    60   ~ 0
finger31
Text Label 2450 5100 0    60   ~ 0
finger32
Text Label 3700 700  2    60   ~ 0
finger1
Text Label 3700 800  2    60   ~ 0
finger2
Text Label 3700 900  2    60   ~ 0
finger3
Text Label 3700 1000 2    60   ~ 0
finger4
Text Label 3700 1100 2    60   ~ 0
finger5
Text Label 4600 2200 2    60   ~ 0
finger6
Text Label 4600 2300 2    60   ~ 0
finger7
Text Label 4600 2400 2    60   ~ 0
finger8
Text Label 4600 2500 2    60   ~ 0
finger9
Text Label 4600 2600 2    60   ~ 0
finger10
Text Label 4600 2700 2    60   ~ 0
finger11
Text Label 4600 2800 2    60   ~ 0
finger12
Text Label 4600 2900 2    60   ~ 0
finger13
Text Label 4600 3350 2    60   ~ 0
finger14
Text Label 4600 3450 2    60   ~ 0
finger15
Text Label 4600 3550 2    60   ~ 0
finger16
Text Label 4600 3650 2    60   ~ 0
finger17
Text Label 4600 3750 2    60   ~ 0
finger18
Text Label 4600 3850 2    60   ~ 0
finger19
Text Label 4600 3950 2    60   ~ 0
finger20
Text Label 4600 4050 2    60   ~ 0
finger21
Text Label 4600 4500 2    60   ~ 0
finger22
Text Label 4600 4600 2    60   ~ 0
finger23
Text Label 4600 4700 2    60   ~ 0
finger24
Text Label 4600 4800 2    60   ~ 0
finger25
Text Label 4600 4900 2    60   ~ 0
finger26
Text Label 4600 5000 2    60   ~ 0
finger27
Text Label 4600 5100 2    60   ~ 0
finger28
Text Label 4600 5200 2    60   ~ 0
finger29
Text Label 3700 1200 2    60   ~ 0
finger30
Text Label 3700 1300 2    60   ~ 0
finger31
Text Label 3700 1400 2    60   ~ 0
finger32
Text Label 7000 2050 2    60   ~ 0
finger1
Text Label 7000 2150 2    60   ~ 0
finger2
Text Label 7000 2250 2    60   ~ 0
finger3
Text Label 7000 2350 2    60   ~ 0
finger4
Text Label 7000 2450 2    60   ~ 0
finger5
Text Label 7000 2550 2    60   ~ 0
finger6
Text Label 7000 2650 2    60   ~ 0
finger7
Text Label 7000 2750 2    60   ~ 0
finger8
Text Label 7000 2850 2    60   ~ 0
finger9
Text Label 7000 2950 2    60   ~ 0
finger10
Text Label 7000 3050 2    60   ~ 0
finger11
Text Label 7000 3150 2    60   ~ 0
finger12
Text Label 7000 3250 2    60   ~ 0
finger13
Text Label 7000 3350 2    60   ~ 0
finger14
Text Label 7000 3450 2    60   ~ 0
finger15
Text Label 7000 3550 2    60   ~ 0
finger16
Text Label 7000 3650 2    60   ~ 0
finger17
Text Label 7000 3750 2    60   ~ 0
finger18
Text Label 7000 3850 2    60   ~ 0
finger19
Text Label 7000 3950 2    60   ~ 0
finger20
Text Label 7000 4050 2    60   ~ 0
finger21
Text Label 7000 4150 2    60   ~ 0
finger22
Text Label 7000 4250 2    60   ~ 0
finger23
Text Label 7000 4350 2    60   ~ 0
finger24
Text Label 7000 4450 2    60   ~ 0
finger25
Text Label 7000 4550 2    60   ~ 0
finger26
Text Label 7000 4650 2    60   ~ 0
finger27
Text Label 7000 4750 2    60   ~ 0
finger28
Text Label 7000 4850 2    60   ~ 0
finger29
Text Label 7000 4950 2    60   ~ 0
finger30
Text Label 7000 5050 2    60   ~ 0
finger31
Text Label 7000 5150 2    60   ~ 0
finger32
Wire Bus Line
	3200 1450 2900 1450
Wire Bus Line
	2900 1450 2900 6050
Wire Bus Line
	2900 6050 6550 6050
Wire Bus Line
	6550 6050 6550 2000
Wire Bus Line
	3200 2050 4200 2050
Wire Bus Line
	4200 2050 4200 5300
Wire Bus Line
	5600 1700 5600 5400
Wire Bus Line
	7850 1700 7850 5350
Wire Bus Line
	9900 1700 9900 5200
$Comp
L SW_DIP_x08 SWouter1
U 1 1 59BC8EB2
P 4000 1100
F 0 "SWouter1" H 4000 1650 50  0000 C CNN
F 1 "SW_DIP_x08" H 4000 650 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_DIP_x8_W6.15mm_Slide_Omron_A6H" H 4000 1100 50  0001 C CNN
F 3 "" H 4000 1100 50  0001 C CNN
	1    4000 1100
	1    0    0    -1  
$EndComp
Wire Bus Line
	3200 2050 3200 650 
Wire Bus Line
	4750 1700 9900 1700
Wire Bus Line
	4750 1700 4750 600 
$Comp
L Conn_02x32_Odd_Even Jmitm1
U 1 1 59BC98A9
P 7200 3550
F 0 "Jmitm1" H 7250 5150 50  0000 C CNN
F 1 "Conn_02x32_Odd_Even" H 7250 1850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x32_Pitch2.54mm_SMD" H 7200 3550 50  0001 C CNN
F 3 "" H 7200 3550 50  0001 C CNN
	1    7200 3550
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SWrst1
U 1 1 59BEB6C6
P 1450 1050
F 0 "SWrst1" H 1500 1150 50  0000 L CNN
F 1 "SW_Push" H 1450 990 50  0000 C CNN
F 2 "brunoeagle-open-modules:PTS645SM43SMTR92" H 1450 1250 50  0001 C CNN
F 3 "" H 1450 1250 50  0001 C CNN
	1    1450 1050
	1    0    0    -1  
$EndComp
Text Label 1650 1050 0    60   ~ 0
finger30
Text Label 1250 1050 2    60   ~ 0
finger32
$Comp
L R R1
U 1 1 59BEC2FD
P 10000 4900
F 0 "R1" V 10080 4900 50  0000 C CNN
F 1 "10K" V 10000 4900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 4900 50  0001 C CNN
F 3 "" H 10000 4900 50  0001 C CNN
	1    10000 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	9550 4900 9850 4900
$EndSCHEMATC
